require 'mkmf'

pkg_config('libebook-1.2') || pkg_config('libebook-1.0')
pkg_config('libecal-1.2') || pkg_config('libecal-1.0')
pkg_config('evolution-data-server-1.2') || pkg_config('evolution-data-server-1.0')
pkg_config('glib-2.0')

$CPPFLAGS += " -Wall"
#$CPPFLAGS += " -W -Wwrite-strings -Waggregate-return -Wnested-externs -Wundef -Wpointer-arith -Wcast-align -Wshadow -Werror"

create_makefile("revolution")

